package main;

import com.vivek.vertx.SenderVerticle.LaucherVertical;
import io.vertx.core.Vertx;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Starter {
     public static void main(String[] args){
        final Vertx vertx = Vertx.vertx();
        vertx.deployVerticle(new LaucherVertical(), res -> {
            if (res.succeeded()) {
                log.
                System.out.println("Deployment id is: " + res.result());
            } else {
                System.out.println("Deployment failed!");
            }
        });
    }
}