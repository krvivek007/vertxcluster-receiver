package com.vivek.vertx.SenderVerticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import main.Constants;

public class ReceiverVertical extends AbstractVerticle {

    private static final String DEFAULT_REPLY_MESSAGE = "Reply message: " ;

    @Override
    public void start() throws Exception {
        final EventBus eventBus = vertx.eventBus();
        eventBus.consumer(Constants.ADDRESS, receivedMessage -> {
            System.out.println("Received message: " + receivedMessage.body());
            //receivedMessage.reply(DEFAULT_REPLY_MESSAGE + receivedMessage);
        });
        System.out.println("Receiver ready!");
    }
}
